#!/bin/ash

apk update
apk add --update npm nodejs git jq

# Instructions taken from https://github.com/IrosTheBeggar/mStream

git clone https://github.com/IrosTheBeggar/mStream.git
cd mStream

npm install
npm link 

JF=save/conf/default.json
[ -f ${JF} ] || echo "{}" > ${JF}
jq -a '.+{ "port": "80", "folders": { "music": { "root": "/home/alpine/Music/" } } }' ${JF} > ${JF}.swap
mv ${JF}.swap ${JF}
cat ${JF} | jq

cat > /etc/init.d/mStream <<EOF
#!/sbin/openrc-run

pidfile=/var/run/node.pid
procname=/usr/bin/node

start() {
		cd /home/alpine/setup/mStream
        ebegin "Starting \${SVCNAME}"
        start-stop-daemon --start --pidfile \${pidfile} --exec node cli-boot-wrapper.js &
        eend \$?
}

stop() {
        ebegin "Stopping \${SVCNAME}"
        killall node
        eend \$?
}

restart() {
	stop
	wait;
	start
}

status() {
	echo "Need a node status command here."
}

EOF
chmod a+x /etc/init.d/mStream
rc-update add mStream
rc-service mStream start

# this did not work
# nohup node cli-boot-wrapper.js &
