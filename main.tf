provider "aws" {
	region		= var.region
	access_key	= var.access_key
	secret_key	= var.secret_key
}

# module "iam" {
# 	source = "./iam"
# }

module "instance" {
	source            	= "./instance"
	# app_db            = module.rds.databaes_db
	instance-username 	= var.instance-username
	music_dir			= var.music_dir
	sgs               	= module.security.sgs
}

# module "rds" {
# 	source	= "./rds"
# 	sgs		= module.security.sgs
# }

module "security" {
	source = "./security"
}

output "public_ip" {
	value = module.instance.PublicIP
}