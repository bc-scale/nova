data "http" "myip" {
	url = "http://ipv4.icanhazip.com"
}

# This AMI is eligable for free tier when used with t2.micro and no EBS optimization.
# Alpine Linux 3.13.0 x86_64 r0 - https://alpinelinux.org/cloud
variable "alpine_linux" {
	default = "ami-08177f9696f9ad4a6"
}

resource "aws_key_pair" "ssh-key" {
	key_name	= "ssh-key"
	public_key	= file(var.ssh_keys.public_key)
}

resource "aws_instance" "alx" {
	ami                         = var.alpine_linux
	#availability_zone           =
	#ebs_optimized               =
	instance_type               = "t2.micro"
	monitoring                  = false
	key_name                    = "ssh-key"
	#subnet_id                   =
	# depends_on = [	]
	vpc_security_group_ids      = [
		var.sgs["ssh-server"],
		var.sgs["web-server"],
		var.sgs["web-client"],
	]
	associate_public_ip_address = true
	source_dest_check           = true
	disable_api_termination     = false
	#private_ip                  =

	root_block_device {
		volume_type           = "gp2"
		volume_size           = 10
		delete_on_termination = false
	}

	# This will enable SSH connections only from your current IP address so it's not exposed to the world.
	provisioner "local-exec" {
		command = "aws ec2 authorize-security-group-ingress --group-id ${var.sgs["ssh-server"]} --cidr \"${chomp(data.http.myip.body)}/32\" --protocol tcp --port 22 ||:"
	}

	provisioner "file" {
		source      = var.music_dir
		destination = "/home/alpine/Music"

		connection {
			type        = "ssh"
			host        = self.public_ip
			user        = var.instance-username
			private_key	= file(var.ssh_keys.private_key)
		}
	}

	provisioner "file" {
		source      = "./setup"
		destination = "/home/alpine"

		connection {
			type        = "ssh"
			host        = self.public_ip
			user        = var.instance-username
			private_key	= file(var.ssh_keys.private_key)
		}
	}

	# Pass Terraform variables to Setup.sh where it uses sed to configure services.
	provisioner "remote-exec" {
		inline = [
			"cd /home/alpine/setup/",
			"sudo /bin/ash ./Setup.sh"
		]

		connection {
			type        = "ssh"
			host        = self.public_ip
			user        = var.instance-username
			private_key	= file(var.ssh_keys.private_key)
			script_path = "/home/alpine/tmpSetup.sh"
		}
	}

	# This will disable SSH connections after the instance is built if SSH to the instance is not needed.
	# provisioner "local-exec" {
	# 	command = "aws ec2 revoke-security-group-ingress --group-id ${var.sgs["ssh-server"]} --cidr \"${chomp(data.http.myip.body)}/32\" --protocol tcp --port 22"
	#}
}

output "PublicIP" {
	value = aws_instance.alx.public_ip
}