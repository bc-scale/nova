variable "access_key" {
	description = "The access key"
}

variable "music_dir" {
	description = "Enter the path to a folder with less then 8GB of music."
}

variable "instance-username" {
	default     = "alpine"
	description = "The admin shell account in the EC2"
}

variable "region" {
	description = "The region"
}

variable "secret_key" {
	description = "The secret key"
}
