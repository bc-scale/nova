# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/aws" {
  version     = "3.29.1"
  constraints = "~> 3.27"
  hashes = [
    "h1:vFaEB2VBkZnKUXuVbOLbq6a8mOYNx2ftmTRP0YVbzu8=",
    "zh:2376f60f59d9c49277ea18d02f6ae316255df214046be87eacdad6ed8e56ba10",
    "zh:48dfd5ad70f26d794911413db14d957abb0bdff5000d5cb486464156201fcdf2",
    "zh:8aec3e84117df4195372ef58a2fd52900338156725da24d62fe7daab14d3ecae",
    "zh:b51fcbc7f2a6d78dfccb82b827f7f9da1ac1e51a5b7b3784f36de3e53b0e963d",
    "zh:c3df4b044020e4a5e13d6532d4e0ba191719b87131ccee07bd35dc819a09412d",
    "zh:c5195102a6411db6a5b96492f5dbe5a637e8063365efc14d13b05cb5e2764e8c",
    "zh:d57da72f95d0d8498d1c42a62241c93c5e30538471585177df6ea9280bf55de3",
    "zh:dec04bb83e3b6424f4e18ce0c48c14f3c75fbdd0d4f9181cb56f846fa9ac51f9",
    "zh:e9b4c360d6572db70e29be95076f2543a562420262c906cff710e662d6223247",
    "zh:ecb606aee05da1b2033874b5a9916d6d2877e748d5478a0d70b6fc4b5d08ab11",
  ]
}

provider "registry.terraform.io/hashicorp/http" {
  version = "2.1.0"
  hashes = [
    "h1:HmUcHqc59VeHReHD2SEhnLVQPUKHKTipJ8Jxq67GiDU=",
    "zh:03d82dc0887d755b8406697b1d27506bc9f86f93b3e9b4d26e0679d96b802826",
    "zh:0704d02926393ddc0cfad0b87c3d51eafeeae5f9e27cc71e193c141079244a22",
    "zh:095ea350ea94973e043dad2394f10bca4a4bf41be775ba59d19961d39141d150",
    "zh:0b71ac44e87d6964ace82979fc3cbb09eb876ed8f954449481bcaa969ba29cb7",
    "zh:0e255a170db598bd1142c396cefc59712ad6d4e1b0e08a840356a371e7b73bc4",
    "zh:67c8091cfad226218c472c04881edf236db8f2dc149dc5ada878a1cd3c1de171",
    "zh:75df05e25d14b5101d4bc6624ac4a01bb17af0263c9e8a740e739f8938b86ee3",
    "zh:b4e36b2c4f33fdc44bf55fa1c9bb6864b5b77822f444bd56f0be7e9476674d0e",
    "zh:b9b36b01d2ec4771838743517bc5f24ea27976634987c6d5529ac4223e44365d",
    "zh:ca264a916e42e221fddb98d640148b12e42116046454b39ede99a77fc52f59f4",
    "zh:fe373b2fb2cc94777a91ecd7ac5372e699748c455f44f6ea27e494de9e5e6f92",
  ]
}
